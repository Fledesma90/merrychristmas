using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManagement : MonoBehaviour
{
    public string scene;
    [SerializeField] GameObject faderIn;
    [SerializeField] GameObject faderOut;
    CanvasGroup canvasGroup;
    [SerializeField] float fadeMultiplier;
    bool canFade=true;
    public GameObject tutorial;

    public GameObject credits;

    
    // Start is called before the first frame update
    void Start()
    {
        canvasGroup = faderIn.GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canFade==true) {
            StartCoroutine(FadeIn());
        }
    }

    public void ChangeScene() {

        StartCoroutine(ChangeToGameWithFade());
        
    }
    /// <summary>
    /// corrutine to make a fadeIn when loading the scene
    /// we have to deactivate it to be able to use the HUD buttons
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn() {
        canvasGroup.alpha -= Time.deltaTime * fadeMultiplier;
        yield return new WaitForSeconds(8);
        canFade = false;
        faderIn.SetActive(false);
    }

    IEnumerator ChangeToGameWithFade() {
        faderOut.SetActive(true);
       
        yield return new WaitForSeconds(7);
        SceneManager.LoadScene("Game");
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void EnableTutorial() {
        tutorial.SetActive(true);
    }

    public void DisableTutorial() {
        tutorial.SetActive(false);
    }
    public void EnableCredits() {
        credits.SetActive(true);
    }

    public void DisableCredits() {
        credits.SetActive(false);
    }

    public void SpanishLanguage() {
        GameManager.instance.isSpanish = true;
        GameManager.instance.isEnglish = false;
    }

    public void EnglishLanguage() {
        GameManager.instance.isSpanish = false;
        GameManager.instance.isEnglish = true;
    }
}
