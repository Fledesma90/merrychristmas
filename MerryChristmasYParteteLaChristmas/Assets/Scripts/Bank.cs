using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class Bank : MonoBehaviour
{
    [SerializeField] int startingBalance = 150;
    [SerializeField] int currentBalance;
    //we can access currentBalance outside the script with this property
    //but we cannot set it
    public int CurrentBalance {get { return currentBalance; } }
    [SerializeField] TextMeshProUGUI displayBalance;
    private void Awake() {
        currentBalance = startingBalance;
        UpdateDisplay();

    }
    private void Start() {
//Add extra food to starting balance in the next round
        if (GameManager.instance.roundNumber>0&&GameManager.instance.bonusFood>0) {
            currentBalance += GameManager.instance.bonusFood;
            GameManager.instance.prueba++;
            UpdateDisplay();
        }
        
    }
    public void Deposit(int amount) {
        //put mathf.abs avoid negative numbers, it gives absolute numbers
        currentBalance += Mathf.Abs(amount);
        UpdateDisplay();
    }
    public void Withdraw(int amount) {
        //put mathf.abs avoid negative numbers, it gives absolute numbers
        currentBalance -= Mathf.Abs(amount);
        UpdateDisplay();
        //if we hit negative balance we lose the game
        if (currentBalance<0) {
            //Lose the game
            ReloadScene();
        }
    }

    void UpdateDisplay() {
        if (GameManager.instance.isEnglish) {
            displayBalance.text = "Cat food" + " " + currentBalance;
        }
        if (GameManager.instance.isSpanish) {
            displayBalance.text = "Comida gatos" + " " + currentBalance;
        }
        
    }
    public void ReloadScene() {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }
}
