using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackCat : MonoBehaviour
{
    //cost of placing the cat
    [SerializeField] int cost = 75;
    FoodManager foodManager;
    private void Start() {
        foodManager = FindObjectOfType<FoodManager>();
    }
    private void Update() {
        //If the round is finished we destroy the cats
        if (GameManager.instance.finishedRound==true) {
            Destroy(gameObject);
        }

        if (foodManager.frozenFood==true) {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Method to instantiate BlackCat
    /// </summary>
    /// <param name="blackCat"></param>
    /// <param name="position"></param>
    public bool CreateBlackCat(BlackCat blackCat, Vector3 position) {
        Bank bank = FindObjectOfType<Bank>();
        if (bank==null) {
            return false;
        }
        if (bank.CurrentBalance>=cost) {
            Instantiate(blackCat.gameObject, position, Quaternion.identity);
            bank.Withdraw(cost);
            //if we hace enough money we will return true,if not false
            return true;
        }
        //if all of the ifs above fail we return false and in this way we won't get
        //an error message 
        return false;
    }
}
