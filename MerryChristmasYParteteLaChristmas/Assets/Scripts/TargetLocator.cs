using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLocator : MonoBehaviour
{
    Transform target;
    [SerializeField] float range = 25f;
    [SerializeField] ParticleSystem particleAttack;

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.finishedRound==false) {
            FindClosestTarget();
            LookAtTarget();
        }
       
    }
    /// <summary>
    /// Find every target in the scene and compare
    /// distances to see which is closest
    /// </summary>
    void FindClosestTarget() {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        //we put it to null because we haven't found one yet
        Transform closestTarget = null;
        //No matter the distance, this is a huge number/distance and it is
        //going to be checked from the beginning
        float maxDistance = Mathf.Infinity;
        foreach (Enemy enemy in enemies) {
            float targetDistance = Vector3.Distance(transform.position,enemy.transform.position);

            if (targetDistance<maxDistance) {
                closestTarget = enemy.transform;
                maxDistance = targetDistance;
            }
        }
        target = closestTarget;
    }
    void LookAtTarget() {
        float targetDistance = Vector3.Distance(transform.position, target.position);
        transform.LookAt(target);

        if (targetDistance<range) {
            Attack(true);
        } else {
            Attack(false);
        }
    }
    /// <summary>
    /// Controls the emissionModulo with the isActive parameter
    /// </summary>
    /// <param name="isActive"></param>
    void Attack(bool isActive) {
       
    ParticleSystem.EmissionModule emissionModule = particleAttack.emission;
        emissionModule.enabled = isActive;


        
    }
}
