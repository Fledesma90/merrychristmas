using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    public GameObject objectPool;
    public bool cinematicsOn;
    public bool canReload;
    public string gameScene = "Game";
    //camera to activate when we first load the round 0
    public GameObject cinematic1;
    //camera to activate when we load the scene and roundNumber>0
    public GameObject foodCamera;
    public GameObject boardCamera;
    public float timeForFirstCinematic;
    bool canActivatePool;
    [SerializeField] GameObject fadeImage;
    CanvasGroup canvasGroup;
    bool canFadeFinish = false;
    // Start is called before the first frame update
    void Start() {
        SetCatsToOff();
        canvasGroup = fadeImage.GetComponent<CanvasGroup>();
        if (GameManager.instance.roundNumber == 0) {
            StartCoroutine(FirstCinematic());
        }
        if (GameManager.instance.roundNumber != 0) {
            StartCoroutine(RegularCinematics());
        }

    }

    private static void SetCatsToOff() {
        GameManager.instance.blackCatOn = false;
        GameManager.instance.siamesOn = false;
        GameManager.instance.whiteBlackCatOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        //we only reload the level only if the round is less than 3
        if (GameManager.instance.finishedRound && GameManager.instance.roundNumber < 3) {
            StartCoroutine(WinRoundCinematic());
        }
        if (canReload==true) {
            ReloadScene();
        }

        if (canFadeFinish==true) {
            canvasGroup.alpha += Time.deltaTime;
        }
    }
    IEnumerator FirstCinematic() {
        GameManager.instance.cinematicsOn = true;
        cinematic1.SetActive(true);
        yield return new WaitForSeconds(timeForFirstCinematic);
        cinematic1.SetActive(false);
        StartCoroutine(RegularCinematics());
       
    }
    IEnumerator RegularCinematics() {
        foodCamera.SetActive(true);
        GameManager.instance.cinematicsOn = true;
        yield return new WaitForSeconds(timeForFirstCinematic);
        
        foodCamera.SetActive(false);
        //TODO activate objectpool after testing with cinematics
        //objectPool.SetActive(true);
        yield return new WaitForSeconds(timeForFirstCinematic);
        GameManager.instance.cinematicsOn = false;
        objectPool.SetActive(true);
    }
    IEnumerator WinRoundCinematic() {
        yield return new WaitForSeconds(5);
        fadeImage.SetActive(true);
        canvasGroup.alpha += Time.deltaTime*0.3f;
        yield return new WaitForSeconds(7);
        canReload = true;

    }
    void ReloadScene() {
        
        GameManager.instance.finishedRound = false;
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
        canReload = false;
    }

   
    /// <summary>
    /// Come back to main menu setting the GameManager with
    /// default settings
    /// </summary>
    /// <param name="scene"></param>
    public void ChangeSceneToMainMenu() {
        GameManager.instance.roundNumber = 0;
        GameManager.instance.finishedRound = false;
        

        StartCoroutine(ChangeSceneFade());
        
    }

    IEnumerator ChangeSceneFade() {
        fadeImage.SetActive(true);
        canFadeFinish = true;
        yield return new WaitForSeconds(7);
        SceneManager.LoadScene("MainMenu");
    }
}
