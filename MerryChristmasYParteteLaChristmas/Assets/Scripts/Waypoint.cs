using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    //in a towerdefense it would be the tower
    [SerializeField] BlackCat allyPrefab;
    [SerializeField] Manxita manxitaPrefab;
    [SerializeField] Siames siamesPrefab;
    FoodManager foodManager;
    //we set a bool to check if there's an object already placed so we can place
    //an object or not
    [SerializeField] bool isPlaceable;

    private void Start() {
        foodManager = FindObjectOfType<FoodManager>();
    }

    private void Update() {

    }
    /// <summary>
    /// Create this property, set isplaceable to true and makes isPlaceable
    /// accessible from other scripts by accessing to the method instead 
    /// of the variable itself
    /// </summary>
    /// <returns></returns>
    public bool IsPlaceable { get { return isPlaceable; } }
    private void OnMouseOver() {

        //TODO do somefeedback when the mouse is over a tile
        if (Input.GetMouseButtonDown(0)&& isPlaceable && GameManager.instance.cinematicsOn!=true
           && GameManager.instance.canPlaceAllies!=false && GameManager.instance.blackCatOn==true
           && foodManager.frozenFood!=true) {
            
            bool isPlaced = allyPrefab.CreateBlackCat(allyPrefab, transform.position);

            //Instantiate(allyPrefab, transform.position,Quaternion.identity);
            //set isplaceable to isPlaced so no more than one object is instantiated
            isPlaceable = ! isPlaced;
        }
        if (Input.GetMouseButtonDown(0) && isPlaceable && GameManager.instance.cinematicsOn != true
           && GameManager.instance.canPlaceAllies != false && GameManager.instance.siamesOn==true
           && foodManager.frozenFood != true) {

            bool isPlaced = siamesPrefab.CreateSiames(siamesPrefab, transform.position);

            //Instantiate(allyPrefab, transform.position,Quaternion.identity);
            //set isplaceable to isPlaced so no more than one object is instantiated
            isPlaceable = !isPlaced;
        }
        if (Input.GetMouseButtonDown(0) && isPlaceable && GameManager.instance.cinematicsOn != true
         && GameManager.instance.canPlaceAllies != false && GameManager.instance.whiteBlackCatOn==true
         && foodManager.frozenFood != true) {

            bool isPlaced = manxitaPrefab.CreateManxita(manxitaPrefab, transform.position);

            //Instantiate(allyPrefab, transform.position,Quaternion.identity);
            //set isplaceable to isPlaced so no more than one object is instantiated
            isPlaceable = !isPlaced;
        }
    }
}
