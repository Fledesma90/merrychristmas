using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    //reference to image of going through a round
    [SerializeField] GameObject winDialogue;

    //reference to image of winning the game
    [SerializeField] GameObject finishGameDialogue;

    [SerializeField] GameObject blackyImage;
    [SerializeField] GameObject andreImage;
    [SerializeField] GameObject manxitaImage;
    [SerializeField] GameObject fadeInGO;
    CanvasGroup canvasGroup;
    [SerializeField] float fadeMultiplier=0.2f;
    public float timeToStopFade;
    [SerializeField] bool canFade =true;
    // Start is called before the first frame update
    void Start()
    {
        canvasGroup = fadeInGO.GetComponent<CanvasGroup>();
        winDialogue = GameObject.Find("DialogueForCongrats");
        winDialogue.SetActive(false);
        finishGameDialogue = GameObject.Find("FinishedGame");
        finishGameDialogue.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (canFade!=false) {
            StartCoroutine(FadeIn());
        }
        
        //canvasGroup.alpha -= Time.deltaTime;
        if (GameManager.instance.finishedRound && GameManager.instance.roundNumber<3) {
            winDialogue.SetActive(true);
        } 
        //if we've finished the round and the round number is equal or bigger
        //than 3, then we win the game and activate the finishgamedialogue
        if (GameManager.instance.finishedRound && GameManager.instance.roundNumber >= 3) {
            finishGameDialogue.SetActive(true);
        }
    }

    public void BlackyOn() {
        blackyImage.SetActive(true);
        andreImage.SetActive(false);
        manxitaImage.SetActive(false);
        GameManager.instance.blackCatOn = true;
        GameManager.instance.siamesOn = false;
        GameManager.instance.whiteBlackCatOn = false;
    }

    public void SaponOn() {
        blackyImage.SetActive(false);
        andreImage.SetActive(true);
        manxitaImage.SetActive(false);
        GameManager.instance.blackCatOn = false;
        GameManager.instance.siamesOn = true;
        GameManager.instance.whiteBlackCatOn = false;
    }

    public void ManxitaOn() {
        blackyImage.SetActive(false);
        andreImage.SetActive(false);
        manxitaImage.SetActive(true);
        GameManager.instance.blackCatOn = false;
        GameManager.instance.siamesOn = false;
        GameManager.instance.whiteBlackCatOn = true;
    }
    /// <summary>
    /// corrutine to make a fadeIn when loading the scene
    /// we have to deactivate it to be able to use the HUD buttons
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn() {
        canvasGroup.alpha -= Time.deltaTime*fadeMultiplier;
        yield return new WaitForSeconds(8);
        canFade = false;
        fadeInGO.SetActive(false);
    }
    //public IEnumerator Fader() {
    //    canvasGroup.alpha += Time.deltaTime;

    //    yield return new WaitForSeconds(5f);

    //    canvasGroup.alpha -= Time.deltaTime;
    //    canIncrease = false;
    //}

    //private void OnTriggerEnter2D(Collider2D collision) {
    //    if (collision.tag == "Player") {
    //        canIncrease = true;
    //        //StartCoroutine(Fader());
    //        GetComponent<Collider2D>().enabled = false;
    //    }
    }
