using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    [SerializeField]public bool cinematicsOn;
    //Extra food that is going to be added to the script bank of currentBalance
    int startingBonusFood;
    public int bonusFood;
    public bool finishedRound;
    public bool canReload;
    public static GameManager instance;
    public int roundNumber;
    private float timeForCinematics=9;
    public string gameScene = "Game";
    //bool to allow substrction of bonusFood
    public int prueba;
    //a bool that is going to be activated on the objectpool
    //and deactivated when all enemies are dead so no animal can be placed
    public bool canPlaceAllies;
    //bool to indicate black cat is on
    public bool blackCatOn;
    //bool to indicate white and black cat is on
    public bool whiteBlackCatOn;
    //bool to indicate siames cat is on
    public bool siamesOn;
    //reference to bank script
    Bank bank;
    public bool isSpanish;
    public bool isEnglish;
    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
        //hacemos que no se destruya cuando se carga otra escena
        DontDestroyOnLoad(this.gameObject);
        
    }
    // Update is called once per frame
    void Update()
    {
        if (finishedRound==true) {
            Bank bank = FindObjectOfType<Bank>();
            //extrafood adds the remaining food
            bonusFood =bank.CurrentBalance;
        } else {
            return;
        }
    } 
}
