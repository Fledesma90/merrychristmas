using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    
    //number of enemies we're going to have in the object pool
    [SerializeField] int poolSize = 4;
    [SerializeField] float spawnTimer=1f;

    GameObject[] pool;
    public int remainingEnemies;
    public bool canSpawnEnemies;
    //bool to fix the issue of instantiate objects again
    public bool justStart;
    float disableJustStart;
    FoodManager foodManager;
    private void Awake() {
        foodManager = FindObjectOfType<FoodManager>();
        poolSize += GameManager.instance.roundNumber;
        GameManager.instance.canPlaceAllies = true;
        remainingEnemies = poolSize;
        
        PopulatePool();
    }
    // Start is called before the first frame update
    void Start()
    {
        justStart = true;
        canSpawnEnemies = true;
        StartCoroutine(SpawnEnemy());
    }

    private void Update() {
        if (justStart==true) {
            disableJustStart += Time.deltaTime;
        }
        
        if (disableJustStart>6) {
            justStart = false;
        }
        //verify if all enemies are dead
        if (remainingEnemies<=0&& foodManager.currentLives>0 ) {
            GameManager.instance.finishedRound = true;
            GameManager.instance.canPlaceAllies = false;
            canSpawnEnemies = false;
            NewWave();
        }
    }
    /// <summary>
    /// With this method we only populate the array of pool
    /// but we don't use it instantiate the objects, we deactivate first object
    /// of array
    /// </summary>
    void PopulatePool() {
        //initialize array
        pool = new GameObject[poolSize];
        for (int i = 0; i < pool.Length; i++) {
            pool[i] = Instantiate(enemyPrefab, transform);
            pool[i].SetActive(false);
            
        }
    }
    /// <summary>
    /// Verify if the first object of the array is false, if so
    /// we set it to true, then we keep on looping
    /// </summary>
    void EnableObjectInPool() {
        for (int i = 0; i < pool.Length; i++) {
            if (pool[i].activeInHierarchy==false && remainingEnemies>0) {
                pool[i].SetActive(true);
                return;
            }
            
        }
    }

    IEnumerator SpawnEnemy() {
        while (canSpawnEnemies==true && justStart==true) {
            EnableObjectInPool();
            yield return new WaitForSeconds(spawnTimer);
        }
    }
    /// <summary>
    /// Finished the current wave and prepare for the next wave, with an extra enemy
    /// </summary>
    void NewWave() {
        GameManager.instance.roundNumber++;
        poolSize++;
        remainingEnemies = poolSize;
    }
}
