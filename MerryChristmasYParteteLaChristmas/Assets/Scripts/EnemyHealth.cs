using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] float maxHitPoint = 5;
    public float currentHealth;
    Enemy enemy;
    ObjectPool objectPool;
    FoodManager foodManager;
    // Start is called before the first frame update
    void OnEnable()
    {
        
        objectPool = FindObjectOfType<ObjectPool>();
    }

    private void Start() {
        //if we put the health in the on enable function
        //it would be resetting time after time the enemy setactive
        currentHealth = maxHitPoint;
        enemy = GetComponent<Enemy>();
        foodManager = FindObjectOfType<FoodManager>();
    }
    private void Update() {
        if (foodManager.frozenFood==true) {
            gameObject.SetActive(false);
        }
    }
    private void OnParticleCollision(GameObject other) {
        ProcessHit();
    }

    void ProcessHit() {
        currentHealth--;
        if (currentHealth<=0) {
            objectPool.remainingEnemies--;
            gameObject.SetActive(false);
            enemy.RewardGold();
        }
    }
}
