using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindPath2 : MonoBehaviour
{
    [SerializeField] List<Path2> path = new List<Path2>();
    public List<Path2> pathDos = new List<Path2>();
    // Start is called before the first frame update
    void Start()
    {
        Path2[] path2s = FindObjectsOfType<Path2>();
        foreach (Path2 path2 in path2s) {
            pathDos.Add(path2.GetComponent<Path2>());
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
