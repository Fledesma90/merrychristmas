using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaderOutMainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<CanvasGroup>().alpha += Time.deltaTime*0.3f;
    }
}
