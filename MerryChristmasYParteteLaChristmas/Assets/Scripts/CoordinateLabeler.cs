using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

[ExecuteAlways]
public class CoordinateLabeler : MonoBehaviour
{
    [SerializeField] Color defaultColor = Color.white;
    [SerializeField] Color blockedColor = Color.gray;
    TextMeshPro label;
    //vector 2 that uses int
    Vector2Int coordinates = new Vector2Int();
    [SerializeField] Waypoint waypoint;

    private void Awake() {
        label = GetComponent<TextMeshPro>();
        label.enabled = false;
        waypoint = GetComponentInParent<Waypoint>();
        //DisplayCoordinates();
    }
    // Update is called once per frame
    void Update()
    {
        
            //DisplayCoordinates();
            
        
        
        //if (PrefabStageUtility.GetCurrentPrefabStage() == null) {
        //    // Only update the object name if we are not editing the prefab
        //    UpdateObjectName();
            
        //}
        ColorCoordinates();
        ToggleCoordinates();
    }
    /// <summary>
    /// show or hide labels of coordinates
    /// </summary>
    void ToggleCoordinates() {
        if (Input.GetKeyDown(KeyCode.C)) {
            label.enabled = !label.IsActive();
        }
    }
    void ColorCoordinates() {
        if (waypoint.IsPlaceable) {
            label.color = defaultColor;
        } else {
            label.color = blockedColor;
        }
    }

    //void DisplayCoordinates() {

    //    coordinates.x = Mathf.RoundToInt(transform.parent.position.x/UnityEditor.EditorSnapSettings.move.x);
    //    //OJO ponemos .z al final porque el entorno es x y z no es z e y
    //    coordinates.y = Mathf.RoundToInt(transform.parent.position.z/UnityEditor.EditorSnapSettings.move.z);
    //    label.text = coordinates.x + "," + coordinates.y;
        
    //}

    void UpdateObjectName() {
        //convertimos el vector 2 en un string the caracteres
        transform.parent.name = coordinates.ToString();
    }
}
