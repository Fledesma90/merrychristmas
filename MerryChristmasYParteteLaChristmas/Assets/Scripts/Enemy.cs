using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] int goldReward = 25;
    [SerializeField] int goldPenalty = 25;
    private float damage=50;
    Bank bank;
    FoodManager foodManager;
    // Start is called before the first frame update
    void Start()
    {
        bank = FindObjectOfType<Bank>();
        foodManager = FindObjectOfType<FoodManager>();
    }

    public void RewardGold() {
        if (bank==null) {
            return;
        }
        bank.Deposit(goldReward);
    }

    public void StealGold() {
        if (bank==null) {
            return;
        }
        bank.Withdraw(goldPenalty);
    }

    public void Damage() {
        if (foodManager==null) {
            return;
        }
        foodManager.TakeHealth(damage);
    }
}
