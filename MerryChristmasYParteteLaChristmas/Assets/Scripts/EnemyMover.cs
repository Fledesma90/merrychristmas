using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class EnemyMover : MonoBehaviour
{
    //create a list of waypoints called path, and we initialize it.
    [SerializeField] List<Waypoint> path = new List<Waypoint>();
    //[SerializeField] float waitTime = 1f;
    [SerializeField] List<Path2> pathDos = new List<Path2>();
    //We set a range between 0 and a positive number due to it cannot be negative,
    //in that case it will break
    [SerializeField ][Range(0f,5f)] float speed;
   
    public int num;
    Enemy enemy;
    ObjectPool objectPool;
    private void Awake() {
            num= Random.Range(0,10);
    }
    void OnEnable()
    {
        objectPool = FindObjectOfType<ObjectPool>();
        FindPath();
        if (num % 2 == 0) {
           
            StartCoroutine(FollowPath());
        } else {
            StartCoroutine(FollowPath2());
        }
    }

    private void Start() {
        enemy = GetComponent<Enemy>();
    }

    void FindPath() {
        
        if (num%2==0) {
            //clear existing path to add a new one
            path.Clear();
            GameObject parent = GameObject.FindGameObjectWithTag("Path");

            foreach (Transform child in parent.transform) {
                path.Add(child.GetComponent<Waypoint>());
            }
        } else {
            pathDos.Clear();
            GameObject parent2 = GameObject.FindGameObjectWithTag("Path2");
            foreach (Transform child2 in parent2.transform) {
                pathDos.Add(child2.GetComponent<Path2>());
            }
        }
      
    }


    void ReturnToStart() {
        transform.position = path[0].transform.position;
        
    }
    void ReturnToStart2() {
        transform.position = pathDos[0].transform.position;

    }

    IEnumerator FollowPath() {
        foreach (Waypoint waypoint in path) {
            //if we do this (transform.position = waypoint.gameObject.transform.position;)
            //we jump from one point to the other without a middle point 
            Vector3 startPosition = transform.position;
            Vector3 endPosition = waypoint.gameObject.transform.position;
            //this is where the Lerp starts
            float travelPercent = 0f;
            //we're going to comment the coroutine since we're going to do it a different way
            //yield return new WaitForSeconds(waitTime);

            //we look at the next tile to rotate the game object
            //Todo intentar suavizar el giro
            transform.LookAt(endPosition);
            //we use a while loop to update the travelPercent, while travelPercent is less than 1
            //, 1 is the final position, we rise the travelPercent and thus the movement;
            while (travelPercent<1f) {
                travelPercent += Time.deltaTime*speed;
                transform.position = Vector3.Lerp(startPosition, endPosition, travelPercent);
                //we wait to the end of the frame so the coroutine is revisited
                yield return new WaitForEndOfFrame();
            }
        }
      
        enemy.Damage();
        objectPool.remainingEnemies--;
        gameObject.SetActive(false);
        
    }
    IEnumerator FollowPath2() {
        foreach (Path2 path2 in pathDos) {
            //if we do this (transform.position = waypoint.gameObject.transform.position;)
            //we jump from one point to the other without a middle point 
            Vector3 startPosition = transform.position;
            Vector3 endPosition = path2.gameObject.transform.position;
            //this is where the Lerp starts
            float travelPercent = 0f;
            //we're going to comment the coroutine since we're going to do it a different way
            //yield return new WaitForSeconds(waitTime);

            //we look at the next tile to rotate the game object
            //Todo intentar suavizar el giro
            transform.LookAt(endPosition);
            //we use a while loop to update the travelPercent, while travelPercent is less than 1
            //, 1 is the final position, we rise the travelPercent and thus the movement;
            while (travelPercent < 1f) {
                travelPercent += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPosition, endPosition, travelPercent);
                //we wait to the end of the frame so the coroutine is revisited
                yield return new WaitForEndOfFrame();
            }
           
        }
        enemy.Damage();
        objectPool.remainingEnemies--;
        gameObject.SetActive(false);
        
    }
}
