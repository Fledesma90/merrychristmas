using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights : MonoBehaviour
{
    public GameObject yellowLights;
    public GameObject redLights;
    public float timer;
    [SerializeField] float maxTimer=5;
    float halfTimer;
    bool canCount;
    // Start is called before the first frame update
    void Start()
    {
        timer = maxTimer;
        halfTimer = maxTimer / 2;
    }

    // Update is called once per frame
    void Update()
    {
        
        timer -= Time.deltaTime;
        LightSwitch();
    }

    void LightSwitch() {
        if (timer>=halfTimer) {
            yellowLights.SetActive(true);
            redLights.SetActive(false);

        }
        if (timer<halfTimer) {
            yellowLights.SetActive(false);
            redLights.SetActive(true);
        }
        if (timer<=0) {
            timer = maxTimer;
        }
    }
}
