using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manxita : MonoBehaviour
{
    [SerializeField] int cost = 30;
    [SerializeField] int foodReward = 10;
    Bank bank;
    FoodManager foodManager;
    public GameObject ps;
    IEnumerator Start() {
        foodManager = FindObjectOfType<FoodManager>();
        bank = FindObjectOfType<Bank>();
        while (true) {
           
            yield return new WaitForSeconds(10f);
            
            MaxitaPower();
        }
    }
    // Update is called once per frame
    void Update()
    {
        //If the round is finished we destroy the cats
        if (GameManager.instance.finishedRound == true) {
            Destroy(gameObject);
        }

        if (foodManager.frozenFood==true) {
            Destroy(gameObject);
        }
    }

    public bool CreateManxita(Manxita manxita, Vector3 position) {
        Bank bank = FindObjectOfType<Bank>();
        if (bank==null) {
            return false;
        }
        if (bank.CurrentBalance>=cost) {
            Instantiate(manxita.gameObject, position, Quaternion.identity);
            bank.Withdraw(cost);
            return true;
        }
        return true;
    }

    public void MaxitaPower() {
        bank.Deposit(foodReward);
        Instantiate(ps, gameObject.transform.position,gameObject.transform.rotation);
        //TODO animation
        //Todo sound
    }
}
