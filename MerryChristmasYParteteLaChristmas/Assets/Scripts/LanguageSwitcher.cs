using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSwitcher : MonoBehaviour
{
    public GameObject englishObject;
    public GameObject spaObject;


    void Update()
    {
        if (GameManager.instance.isEnglish) {
            englishObject.SetActive(true);
            spaObject.SetActive(false);

        }
        if (GameManager.instance.isSpanish) {
            englishObject.SetActive(false);
            spaObject.SetActive(true);

        }
    }
}
