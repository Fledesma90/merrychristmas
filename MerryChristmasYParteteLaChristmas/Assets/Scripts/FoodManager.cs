using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FoodManager : MonoBehaviour
{
    public float maxLives;
    public float currentLives;
    public Image healthUI;
    public GameObject deadMenu;
    public GameObject [] food;
    public bool frozenFood;
    public GameObject[] ps;
    // Start is called before the first frame update
    void Start()
    {
        currentLives = maxLives;
        deadMenu.SetActive(false);
        if (GameManager.instance.roundNumber==0) {
            food[0].SetActive(true);
        }
        if (GameManager.instance.roundNumber == 1) {
            food[1].SetActive(true);
        }
        if (GameManager.instance.roundNumber == 2) {
            food[2].SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        healthUI.fillAmount = currentLives / maxLives;
    }

    public void TakeHealth(float livesToTake) {
        currentLives -= livesToTake;
        if (currentLives<=0) {
            frozenFood = true;
            foreach (GameObject parsy in ps) {
                parsy.SetActive(false);
            }
            deadMenu.SetActive(true);
        }
    }

    
}
