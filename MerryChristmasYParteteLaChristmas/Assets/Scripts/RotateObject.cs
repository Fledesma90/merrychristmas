using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public float degreesPerSecond = 15.0f;
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();
    public float amplitude = 0.5f;
    public float frequency = 1f;
    // Start is called before the first frame update
    void Start()
    {
        posOffset = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        FloatAndRotateFX();
    }

    private void FloatAndRotateFX() {
        //Rota el objeto en el ejeY
        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        // Flota arriba y abajo 
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = tempPos;
    }
}
