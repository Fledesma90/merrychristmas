using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Siames : MonoBehaviour
{
    [SerializeField] int cost = 100;
    FoodManager foodManager;
    public GameObject ps;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        foodManager = FindObjectOfType<FoodManager>();
        while (true) {
            yield return new WaitForSeconds(10);
            
            AndrePower();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //If the round is finished we destroy the cats
        if (GameManager.instance.finishedRound == true) {
            Destroy(gameObject);
        }

        if (foodManager.frozenFood==true) {
            Destroy(gameObject);
        }
    }

    public bool CreateSiames(Siames siames, Vector3 position) {
        Bank bank = FindObjectOfType<Bank>();
        if (bank==null) {
            return false;
        }

        if (bank.CurrentBalance>=cost) {
            Instantiate(siames.gameObject, position, Quaternion.identity);
            bank.Withdraw(cost);
            return true;
        }
        return false;
    }

    public void AndrePower() {
        if (foodManager.currentLives<100) {
            foodManager.currentLives += 10;
            Instantiate(ps, gameObject.transform.position, gameObject.transform.rotation);
        }
        
    }
}
